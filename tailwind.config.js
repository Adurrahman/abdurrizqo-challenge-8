module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {colors: {
      bgColor: "#F1F3FF",
      sideColor: "#0D28A6",
      grayBg: "#F4F5F7",
      grayFont: "#8A8A8A",
    },},
  },
  plugins: [],
};
