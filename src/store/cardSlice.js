import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  carData: [
    {
      imgCar: "/img/cardsCar.png",
      idCar: "1",
      carName: "Avanza",
      carPrice: "1.0000.000",
      kapasitas: "4",
      statusMobil: true,
    },
    {
      imgCar: "/img/cardsCar.png",
      idCar: "2",
      carName: "Xenia",
      carPrice: "1.000.000",
      kapasitas: "4",
      statusMobil: true,
    },
    {
      imgCar: "/img/cardsCar.png",
      idCar: "3",
      carName: "Tank",
      carPrice: "2.0000.000",
      kapasitas: "2",
      statusMobil: true,
    },
  ],
};

export const carSlice = createSlice({
  name: "carData",
  initialState,
  reducers: {
    addNewCar: (state, action) => {
      let car = {
        idCar: state.carData.length + 1,
        ...action.payload,
      };
      state.carData = [...state.carData, car];
    },
  },
});

export const { addNewCar } = carSlice.actions;

export default carSlice.reducer;
