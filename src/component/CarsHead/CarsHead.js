import React from "react";
import { Link } from "react-router-dom";

function CarsHead() {
  return (
    <>
      <div className="mt-10 flex justify-between items-center">
        <h1>List Cars</h1>
        <Link to={"/dashboard/addnewcar"}>
          <button className="btn btn-primary h-10 px-4">Add New car</button>
        </Link>
      </div>
      <div className="flex justify-between px my-4 w-1/3">
        <button className="btn btn-outline-primary px-4">All</button>
        <button className="btn btn-outline-primary px-4">Small</button>
        <button className="btn btn-outline-primary px-4">Medium</button>
        <button className="btn btn-outline-primary px-4">Large</button>
      </div>
    </>
  );
}

export default CarsHead;
