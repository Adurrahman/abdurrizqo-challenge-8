import React from "react";

function Navdash() {
  return (
    <div className="w-full h-14 bg-white shadow-sm flex justify-between fixed top-0 items-center pr-12 pl-6 ml-20 z-10 ">
      <div>
        <img src="/img/Rectangle2.svg" alt="..." />
      </div>
      <div className="flex items-center p-20 ">
        <img src="/img/circle.svg" alt="..." />
        <div className="font-light pr-6 pl-3">Abdurrizqo Arrahman</div>
      </div>
    </div>
  );
}

export default Navdash;
