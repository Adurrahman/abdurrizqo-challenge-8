import React from "react";

function Sidebar() {
  return (
    <>
      <div className="h-full fixed left-0 top-0 flex  ">
        <div className="bg-sideColor flex-col items-center z-20 w-20">
          <div className="flex flex-col items-center mb-8 mt-2">
            <img src="/img/Rectangle.svg" alt="..." />
          </div>
          <div className="flex flex-col items-center mb-8">
            <img src="/img/fi_home.svg" alt="..." />
            <p className="text-white text-sm">Dashboard</p>
          </div>
          <div className="flex flex-col items-center">
            <img src="/img/fi_truck.svg" alt="..." />
            <p className="text-white text-sm">Cars</p>
          </div>
        </div>

        <div className="bg-white w-56 mt-14">
          <h3 className="py-6 text-grayFont pl-6">Cars</h3>
          <p className="font-bold bg-sideColor/20 py-3 pl-6">List Car</p>
        </div>
      </div>
    </>
  );
}

export default Sidebar;
