import React from "react";

function WhyUs() {
  const dataWhyUs = [
    {
      title: "Mobil Lengkap",
      body: "Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat",
    },

    {
      title: "Layanan 24 Jam",
      body: "Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu",
    },

    {
      title: "Harga Murah",
      body: "Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain",
    },

    {
      title: "Sopir Profesional",
      body: "Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu",
    },
  ];
  return (
    <div className="my-10 container">
      <h1>Why us?</h1>
      <p>Mengapa harus pilih Binar Car Rental?</p>
      <div className="grid grid-cols-4 gap-4">
        {dataWhyUs.map((item, index) => {
          return (
            <div
              key={index}
              className="border-1 px-2 py-3 h-40 border-grayFont/20 rounded"
            >
              <h3>{item.title}</h3>
              <p>{item.body}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default WhyUs;
