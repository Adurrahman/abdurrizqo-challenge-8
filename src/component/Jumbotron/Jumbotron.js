import React from "react";

function Jumbotron() {
  return (
    <div className="flex w-full h-full bg-bgColor">
      <div className="flex-1 mt-56 my-32 ml-32">
        <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
        <p>
          Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
          terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
          untuk sewa mobil selama 24 jam.
        </p>
      </div>
      <div className="flex-1 relative">
        <img
          className="absolute right-0 bottom-0"
          src="img/img_car.png"
          alt="..."
        />
      </div>
    </div>
  );
}

export default Jumbotron;
