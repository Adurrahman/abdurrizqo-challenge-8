import React from "react";

function LeftDetail() {
  return (
    <div className="p-4 my-20 col-span-2 border-1 rounded-xl shadow-sm border-grayFont/30">
      <h3>Tentang Paket</h3>
      <div>
        <h6>include</h6>
        <ol className="list-disc text-grayFont">
          <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
          <li>Sudah termasuk bensin selama 12 jam</li>
          <li>Sudah termasuk Tiket Wisata</li>
          <li>Sudah termasuk pajak</li>
        </ol>
      </div>

      <div>
        <h6>exclude</h6>

        <ol className="list-disc text-grayFont">
          <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
          <li>
            Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
            20.000/jam
          </li>
          <li>Tidak termasuk akomodasi penginapan</li>
        </ol>
      </div>

      <div>
        <h6>Refund, Reschedule, Overtime</h6>

        <ol className="list-disc text-grayFont">
          <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
          <li>
            Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
            20.000/jam
          </li>
          <li>Tidak termasuk akomodasi penginapan</li>
          <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
          <li>
            Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
            20.000/jam
          </li>
          <li>Tidak termasuk akomodasi penginapan</li>
          <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
          <li>
            Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
            20.000/jam
          </li>
        </ol>
      </div>
    </div>
  );
}

export default LeftDetail;
