import React from "react";

function Crads({ carData }) {
  return (
    <div className="bg-white shadow-sm rounded p-3">
      <div>
        <img className="mx-auto my-5" src={carData.imgCar} alt="...." />
      </div>
      <div>
        <p>{carData.carName}</p>
        <h3>Rp {carData.carPrice} / hari</h3>
      </div>
      <div className="my-3">
        <li className="list-none">Start rent - Finish rent</li>
        <li className="list-none">Updated at 4 Apr 2022, 09.00</li>
      </div>
      <div className="flex ">
        <button className="btn btn-outline-danger basis-1/2 mr-2">
          Delete
        </button>
        <button className="btn btn-success basis-1/2 ml-2">edit</button>
      </div>
    </div>
  );
}

export default Crads;
