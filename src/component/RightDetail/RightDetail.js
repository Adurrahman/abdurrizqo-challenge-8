import React from "react";
import ModalImage from "react-modal-image";
import { Link } from "react-router-dom";

function RightDetail({ data }) {
  return (
    <div className="p-4 my-20 border-1 rounded-xl shadow-sm border-grayFont/30">
      <div>
        <ModalImage small={data.imgCar} large={data.imgCar} />

        {/* <img className="mx-auto my-5" src={data.imgCar} alt="...." /> */}
      </div>
      <h3>{data.carName}</h3>
      <div className="grid grid-cols-4 gap-2 text-grayFont">
        <p>4 orang</p>
        <p>Manual</p>
        <p>Tahun 2020</p>
      </div>
      <div className="flex w-full justify-between mt-5">
        <div>Total</div>
        <div className="font-bold">Rp {data.carPrice}</div>
      </div>
      <Link to={`/dashboard/${data.idCar}/invoice`}>
        <button className="btn btn-success w-full mt-5">
          Lanjutkan Pembayaran
        </button>
      </Link>
    </div>
  );
}

export default RightDetail;
