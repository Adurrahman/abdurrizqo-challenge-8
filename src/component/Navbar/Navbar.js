import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div className="w-full h-16 bg-bgColor flex justify-between px-20">
      <div className="flex grow items-center">
        <img src="img/logo.svg" alt="..." />
      </div>
      <div className="flex justify-between w-1/3 items-center">
        <p>Our Services</p>
        <p>Why Us</p>
        <p>Testimonial</p>
        <p>FAQ</p>
        <Link to={`/dashboard`}>
          <button className="btn btn-success my-2">Dashboard</button>
        </Link>
      </div>
    </div>
  );
}

export default Navbar;
