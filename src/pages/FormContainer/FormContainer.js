import React, { useState } from "react";
import { Upload } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { addNewCar } from "../../store/cardSlice";
import { useNavigate } from "react-router-dom";

function FormContainer() {
  const [fileImg, setFileImg] = useState();
  const [newData, setNewData] = useState({});
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleChangeForm = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setNewData((values) => ({ ...values, [name]: value }));
  };

  function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  function beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      console.log("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      console.log("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      setLoading(true);
      return;
    }
    if (info.file.status === "done") {
      getBase64(info.file.originFileObj, (imageUrl) => {
        setFileImg(imageUrl);
        setNewData({ ...newData, imgCar: imageUrl });
        setLoading(false);
      });
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(addNewCar(newData));
    navigate("/dashboard");
  };

  return (
    <div>
      <div className="pt-16 py-20 pl-80 pr-4">
        <div className="mt-10">
          <h1>Add New Car</h1>
        </div>
        <div className="bg-white p-4">
          <form onSubmit={handleSubmit}>
            <div className="mb-4 w-1/3 flex justify-between">
              <label className="">
                Nama <span className="text-danger">*</span>
              </label>
              <input
                name="carName"
                className="px-2 w-56 border-2 rounded border-sideColor/20"
                type="text"
                onChange={handleChangeForm}
              />
            </div>

            <div className="mb-4 w-1/3 flex justify-between">
              <label className="">
                Harga <span className="text-danger">*</span>
              </label>

              <input
                className="px-2 w-56 border-2 rounded border-sideColor/20"
                type="text"
                name="carPrice"
                onChange={handleChangeForm}
              />
            </div>

            <div className="mb-4 w-1/3 flex justify-between">
              <label className="">
                Kapasitas <span className="text-danger">*</span>
              </label>
              <input
                className="px-2 w-56 border-2 rounded border-sideColor/20"
                type="text"
                name="kapasitas"
                onChange={handleChangeForm}
              />
            </div>

            <div className="w-1/3 flex justify-between">
              <label className="">
                Status Mobil <span className="text-danger">*</span>
              </label>
              <input
                className="px-2 w-56 border-2 rounded border-sideColor/20"
                type="text"
                name="statusMobil"
                onChange={handleChangeForm}
              />
            </div>

            <div className="my-12 w-1/3">
              <h5 className="mb-4">Upload Car Image</h5>
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader "
                showUploadList={false}
                action="https://httpbin.org/post"
                beforeUpload={beforeUpload}
                onChange={handleChange}
              >
                <div>
                  {fileImg ? (
                    <img src={fileImg} alt="avatar" style={{ width: "100%" }} />
                  ) : (
                    <>
                      <PlusOutlined />
                      <div style={{ marginTop: 8 }}>Upload</div>
                    </>
                  )}
                </div>
              </Upload>
            </div>
            <div className="text-center w-1/3">
              <button className="btn btn-outline-danger w-40 mr-8">
                cancel
              </button>
              <button className="btn btn-success w-40">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default FormContainer;
