import React from "react";
import Navdash from "../../component/Navdash/Navdash";
import Sidebar from "../../component/Sidebar/Sidebar";
function Dashboard({ pageControl }) {
  return (
    <div className="bg-grayBg w-full h-full">
      <Navdash />
      {pageControl}
      <Sidebar />
    </div>
  );
}

export default Dashboard;
