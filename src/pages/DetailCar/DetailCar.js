import React from "react";
import { useParams } from "react-router-dom";
import Navbar from "../../component/Navbar/Navbar";
import LeftDetail from "../../component/LeftDetail/LeftDetail";
import RightDetail from "../../component/RightDetail/RightDetail";
import { useSelector } from "react-redux";

function DetailCar() {
  const param = useParams();
  const carData = useSelector((state) =>
    state.carsData.carData.find((item) => item.idCar == param.id)
  );
  const tes = useSelector((state) => state.carsData.carData);

  console.log(tes);

  return (
    <>
      <Navbar />
      <div className="grid grid-cols-3 gap-3 container">
        <LeftDetail />
        <RightDetail data={carData} />
      </div>
    </>
  );
}

export default DetailCar;
