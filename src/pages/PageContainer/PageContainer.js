import React from "react";
import { useSelector } from "react-redux";
import Crads from "../../component/Cards/Crads";
import CarsHead from "../../component/CarsHead/CarsHead";
import { Link } from "react-router-dom";

function PageContainer() {
  const carsData = useSelector((state) => state.carsData.carData);
  console.log(carsData);
  return (
    <div>
      <div className="pt-16 py-20 pl-80 pr-4">
        <CarsHead />
        <div className="grid grid-cols-3 gap-4">
          {carsData.map((item) => {
            return (
              <Link to={`/dashboard/${item.idCar}`}>
                <Crads carData={item} />
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default PageContainer;
