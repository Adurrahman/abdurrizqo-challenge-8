import React from "react";
import Jumbotron from "../../component/Jumbotron/Jumbotron";
import Navbar from "../../component/Navbar/Navbar";
import PieChart from "../../component/PieChart/PieChart";
import WhyUs from "../../component/WhyUs/WhyUs";

function Home() {
  return (
    <>
      <Navbar />
      <Jumbotron />
      <WhyUs />
      <div className="flex container my-20">
        <div className="basis-2/5 m-auto">
          <PieChart />
        </div>
        <div className="basis-3/5 p-16">
          <h1>Jumlah Mobil Yang Di Miliki</h1>
          <p className="px-16 my-10">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
            molestie arcu dolor. Vivamus non ornare ipsum. Nullam cursus ipsum
            porta, eleifend ipsum vitae, ultrices lorem. Quisque blandit
            ultricies justo, et bibendum orci faucibus et. Nunc porta posuere
            tellus, eget ornare nisl. Proin porta aliquam tortor, vitae lacinia
            lectus fermentum quis. Duis eros neque, scelerisque nec commodo eu,
            cursus in magna. Curabitur vel magna eleifend libero facilisis
            lobortis. Donec ullamcorper sem dictum fringilla porttitor. Nam
            risus mi, dignissim mattis nibh sit amet, commodo maximus neque.
            Duis sodales dolor dolor,
          </p>
        </div>
      </div>
      <div className="w-full h-16 bg-sideColor/10 mt-20"></div>
    </>
  );
}

export default Home;
