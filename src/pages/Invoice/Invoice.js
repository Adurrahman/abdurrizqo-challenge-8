import React from "react";
import Navbar from "../../component/Navbar/Navbar";
import { Worker, Viewer } from "@react-pdf-viewer/core";
import "@react-pdf-viewer/core/lib/styles/index.css";
import pdfFiletes from "../../pdf-open-parameters.pdf";
import { getFilePlugin } from "@react-pdf-viewer/get-file";
import { DownloadOutlined } from "@ant-design/icons";

function Invoice() {
  const getFilePluginInstance = getFilePlugin();
  const { Download } = getFilePluginInstance;
  return (
    <>
      <Navbar />
      <div className="mx-auto my-10 container text-center">
        <div>
          <img className="mx-auto" src="/img/success.svg" alt="..." />
        </div>
        <h4 className="mt-4">Pembayaran Berhasil!</h4>
        <p className="font-thin mt-3">
          Tunjukkan invoice ini ke petugas BCR di titik temu.
        </p>
      </div>

      <div className="w-2/3 mx-auto p-6 rounded-2xl border-2 border-dashed border-grayFont/40">
        <div className="flex justify-between mb-4">
          <h3>Invoice</h3>
          <div>
            <Download>
              {(props) => (
                <button
                  className="p-2 border-2 border-sideColor text-sideColor flex justify-between items-center w-24"
                  onClick={props.onClick}
                >
                  <DownloadOutlined className="text-xl" /> Unduh
                </button>
              )}
            </Download>
          </div>
        </div>
        <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.min.js">
          <div
            style={{
              border: "1px solid rgba(0, 0, 0, 0.3)",
              height: "450px",
            }}
          >
            <Viewer fileUrl={pdfFiletes} plugins={[getFilePluginInstance]} />
          </div>
        </Worker>
      </div>

      <div className="w-full h-16 bg-sideColor/10 mt-20"></div>
    </>
  );
}

export default Invoice;
