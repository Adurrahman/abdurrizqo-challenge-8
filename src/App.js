import { Route, Routes } from "react-router-dom";
import "./App.css";
import Dashboard from "./pages/dashboard/Dashboard";
import DetailCar from "./pages/DetailCar/DetailCar";
import FormContainer from "./pages/FormContainer/FormContainer";
import Home from "./pages/home/Home";
import Invoice from "./pages/Invoice/Invoice";
import PageContainer from "./pages/PageContainer/PageContainer";

function App() {
  return (
    <Routes>
      <Route element={<Home />} path="/" />
      <Route
        element={<Dashboard pageControl={<PageContainer />} />}
        path="/dashboard"
      />
      <Route
        element={<Dashboard pageControl={<FormContainer />} />}
        path="/dashboard/addnewcar"
      />
      <Route element={<DetailCar />} path="/dashboard/:id" />
      <Route element={<Invoice />} path="/dashboard/:id/invoice" />
    </Routes>
  );
}

export default App;
